import random
import string
import os
import time
import math
import sys
from tkinter import *
from PIL import Image


def to_hex(rgb):
    return '#%02x%02x%02x' % rgb

MUTATION_RATE = 0.01
TOTAL_POP = 150
MATING_POOL = [] #arraylist<DNA>
TARGET = 10

im = Image.open('fruit.jpg')

pix = im.load()
WIDTH, HEIGHT = im.size

class Line(Frame):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.master.title("Lines")
        self.pack(fill=BOTH, expand=1)
        self.width = random.randint(1, 4)

        self.x1 = random.random()*WIDTH
        self.y1 = random.random()*HEIGHT
        self.x2 = random.random()*WIDTH
        self.y2 = random.random()*HEIGHT
    
        self.color = to_hex(pix[random.randint(0, WIDTH), random.randint(0, HEIGHT)])
        #self.alpha = 0
        self.fitness = 0.0
        
        
        
    def update_fitness(self):
        score = 0
        for r in range(0, WIDTH, 10):
            for c in range(0, HEIGHT, 20):
                if self.color == pix[r, c]:
                    score+=1;
        self.fitness = float(score)/(WIDTH*HEIGHT)
        
    def crossover(self, partner):
        child = Line()
        coin = random.randint(0, 2)
        if coin == 1:
            child.color = self.color
            child.width = self.width
            child.x1 = self.x1
            child.y1 = self.y1
            child.x2 = self.x2
            child.y2 = self.y2 
        else:
            child.color = partner.color
            child.width = partner.width
            child.x1 = partner.x1
            child.y1 = partner.y1
            child.x2 = partner.x2
            child.y2 = partner.y2
        return child

    def mutate(self, mutationRate):
        if random.random() < MUTATION_RATE:
            x = random.randint(0, 4)
            if x == 0:
                self.color = to_hex(pix[random.randint(0, WIDTH), random.randint(0, HEIGHT)])
            if x == 1:
                self.width = random.randint(1, 4)
            if x == 2:
                self.x1 = random.random()*WIDTH
                self.y1 = random.random()*HEIGHT
            if x == 3:
                self.x2 = random.random()*WIDTH
                self.y2 = random.random()*HEIGHT
                
                

        
population = [Line() for x in range(TOTAL_POP)] 

def setup():
    for i in range(len(population)):
        population[i] = Line()

       
def main():
    l = Line()
    mGui.geometry(str(WIDTH)+'x'+ str(HEIGHT))
    mGui.mainloop()
    
    for i in range(len(population)):
        population[i].update_fitness()
        canvas = Canvas(population[i])
        canvas.create_line(population[i].x1, population[i].y1, population[i].x2, population[i].y2, population[i].width, fill=population[i].color)
        
        canvas.pack(fill=BOTH, expand=1)

    for i in range(len(population)):
        n = int(population[i].fitness*100)
        for j in range(n):
            MATING_POOL.append(population[i])

    for i in range(len(population)):
        a = random.randint(0, len(MATING_POOL)-1)
        b = random.randint(0, len(MATING_POOL)-1)
        while b == a:
            b = random.randint(0, len(MATING_POOL))

        parentA = MATING_POOL[a]
        parentB = MATING_POOL[b]
        child = parentA.crossover(parentB)
        child.mutate(mutationRate)
        population[i] = child

        
setup()
generation = 1
best = population[0]
mGui = Tk() 
while best.fitness < TARGET:
    os.system('clear')
    print("Generation" + str(generation))
    generation += 1
    if __name__ == '__main__':
        main()
    time.sleep(.00001)
    canvas.delete("all")
