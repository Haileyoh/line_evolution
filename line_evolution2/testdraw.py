import sys
from tkinter import *

class Example(Frame):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.master.title("Lines")
        self.pack(fill=BOTH, expand=1)

        canvas = Canvas(self)
        canvas.create_line(15, 25, 200, 25, width=2, fill="#008040") #line
        #canvas.create_line(300, 35, 300, 200, dash=(4, 2)) #dashed line
        #canvas.create_line(55, 85, 155, 85, 105, 180, 55, 85) #triangle

        canvas.pack(fill=BOTH, expand=1)


def main():
    mGui = Tk()
    ex = Example()
    mGui.geometry('450x250+300+300')
    mGui.mainloop()

if __name__ == '__main__':
    main()
