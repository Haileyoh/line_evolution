import random
import string
import os
import time
"""
alphabet = string.ascii_lowercase
truth_value = True
str = ''
while truth_value:
    randint = random.randint(0, 26)
    str += alphabet[randint-1]
    if random.randint(0, 2) == 1:
        truth_value = False
print(str)
"""
mutationRate = 0.01
totalPopulation = 150
matingPool = [] #arraylist<DNA>
target = "to be or not to be"
class DNA:
    def __init__(self):
        self.genes = ['.' for  x in range(18)] #char[18]
        for i in range(len(self.genes)):
            self.genes[i] = chr(random.randint(32, 128))
        self.fitness = 0.0
    def update_fitness(self):
        score = 0
        for i in range(len(self.genes)):
            if self.genes[i] == target[i]:
                score+=1;
        self.fitness = float(score)/len(target)
        
    def crossover(self, partner):
        child = DNA()
        for i in range(len(self.genes)):
            coin = random.randint(0, 2)
            if coin == 1:
                child.genes[i] = self.genes[i]
            else:
                child.genes[i] = partner.genes[i]
        return child

    def mutate(self, mutationRate):
        for i in range(len(self.genes)):
            if random.random() < mutationRate:
                self.genes[i] = chr(random.randint(32, 128))

    def getPhrase(self):
        str1 = ""
        return(str1.join(self.genes)) 

        
population = [DNA() for x in range(totalPopulation)] #DNA[100]

def setup():
    for i in range(len(population)):
        population[i] = DNA()
        
def draw():
    for i in range(len(population)):
        population[i].update_fitness()

    for i in range(len(population)):
        n = int(population[i].fitness*100)
        for j in range(n):
            matingPool.append(population[i])

    for i in range(len(population)):
        a = random.randint(0, len(matingPool)-1)
        b = random.randint(0, len(matingPool)-1)
        while b == a:
            b = random.randint(0, len(matingPool))

        parentA = matingPool[a] #type DNA
        parentB = matingPool[b] #type DNA
        child = parentA.crossover(parentB)
        child.mutate(mutationRate)
        population[i] = child

        
setup()
generation = 1
best = population[0]
while best.getPhrase() != target:
    os.system('clear')
    best = population[0]
    for x in population:
        if x.fitness > best.fitness:
            best = x
    print("Best phrase: " + best.getPhrase())
    print("Generation" + str(generation))
    generation += 1
    draw()
    time.sleep(.00001)
